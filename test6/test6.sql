--创建表空间
CREATE TABLESPACE other_tablespace
DATAFILE 'other_tablespace.dbf'
SIZE 100M
AUTOEXTEND ON
EXTENT MANAGEMENT LOCAL
SEGMENT SPACE MANAGEMENT AUTO;

create table TB_USER
(id  INTEGER,
 no  VARCHAR2(50),
 pwd  VARCHAR2(50),
 name VARCHAR2(50),
 type CHAR(1)
)；

create table TB_ORDER
(
 id      INTEGER,
 commodity_id INTEGER,
 customer_id  INTEGER,
 phone     VARCHAR2(20),
 address    VARCHAR2(100),
 postal_code  VARCHAR2(20),
 order_time  VARCHAR2(50),
 total_amount VARCHAR2(10)
)；

BEGIN
    FOR i IN 1..100000 LOOP
        INSERT INTO OrderDetail (order_detail_id, order_id, product_id, quantity, unit_price)
        VALUES (i, i, ROUND(DBMS_RANDOM.VALUE(1, 100000), 0), ROUND(DBMS_RANDOM.VALUE(1, 10), 0), ROUND(DBMS_RANDOM.VALUE(10, 100), 2));
        COMMIT;
    END LOOP;
END;
/

create table TB_COMMODITY
(
 id      INTEGER,
 product_name VARCHAR2(100),
 classify_id  INTEGER,
 model    VARCHAR2(50),
 unit     VARCHAR2(50),
 market_value VARCHAR2(50),
 sales_price  VARCHAR2(50),
 cost_price  VARCHAR2(50),
 img      VARCHAR2(100),
 introduce   VARCHAR2(500),
 num     INTEGER
);

INSERT INTO TB_USER VALUES ('01', 'admin', '123', 'admin', '1');
INSERT INTO TB_USER VALUES ('02', 'customer01', '123', 'customer02', '2');
INSERT INTO TB_USER VALUES ('02', 'customer02', '123', 'customer02', '3');
INSERT INTO TB_USER VALUES ('02', 'customer03', '123', 'customer02', '4');

select * from TB_USER

DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..10000 LOOP
    DECLARE
      random_category VARCHAR2(50);
    BEGIN
      SELECT '物品' || TO_CHAR(DBMS_RANDOM.VALUE(1, 100)) INTO random_category FROM DUAL;
      INSERT INTO TB_CLASS (id, name) VALUES (snacks_seq.NEXTVAL, random_category);
    END;
  END LOOP;
  COMMIT;
END;
/

![Alt text](5.png)

--为了向"CUSTOMER"表插入虚拟数据，我们可以按照以下步骤进行：使用FOR循环语句，从1到30,000遍历每个整数i。在每次迭代中，使用INSERT INTO语句将一条新记录插入"CUSTOMER"表。这条记录包括字段customerid（即i）、customername（格式为"Customer i"）和email（格式为"customeri@example.com"）。
--同时，使用SELECT语句从"dual"表中选择一行，并使用子查询来检查是否已经存在具有相同customerid的记录。如果存在相同的customerid记录，则不执行插入操作。
BEGIN
  FOR i IN 1..30000 LOOP
    INSERT INTO customer (customerid, customername, email)
    SELECT i AS customerid, 'Customer ' || i AS customername, 'customer' || i || '@example.com' AS email
    FROM dual
    WHERE NOT EXISTS (
      SELECT 1
      FROM customer
      WHERE customerid = i
    );
  END LOOP;
  COMMIT;
END;
/

CREATE OR REPLACE PACKAGE BODY SalesPackage AS
    -- 存储过程：生成订单
    PROCEDURE create_order(p_customer_id NUMBER, p_product_id NUMBER) IS
        v_order_id NUMBER;
        v_quantity NUMBER;
        v_unit_price NUMBER;
    BEGIN
        -- 在Order表中插入新订单
        INSERT INTO "Order" (order_id, customer_id, order_date, status)
        VALUES (order_id_seq.NEXTVAL, p_customer_id, SYSDATE, 'New')
        RETURNING order_id INTO v_order_id;

        -- 获取商品的数量和单价
        SELECT quantity, price INTO v_quantity, v_unit_price
        FROM Product
        WHERE product_id = p_product_id;

        -- 在OrderDetail表中插入订单明细
        INSERT INTO OrderDetail (order_detail_id, order_id, product_id, quantity, unit_price)
        VALUES (order_detail_id_seq.NEXTVAL, v_order_id, p_product_id, v_quantity, v_unit_price);

        UPDATE Product
        SET stock = stock - v_quantity
        WHERE product_id = p_product_id;
    END;

    FUNCTION calculate_order_total(p_order_id NUMBER) RETURN NUMBER IS
        v_total_price NUMBER;
    BEGIN
        SELECT SUM(quantity * unit_price) INTO v_total_price
        FROM OrderDetail
        WHERE order_id = p_order_id;

        RETURN v_total_price;
    END;

    FUNCTION get_customer_purchase_history(p_customer_id NUMBER) RETURN SYS_REFCURSOR IS
        v_cursor SYS_REFCURSOR;
    BEGIN
        OPEN v_cursor FOR
        SELECT od.order_id, p.name, od.quantity, od.unit_price
        FROM OrderDetail od
        INNER JOIN Product p ON od.product_id = p.product_id
        WHERE EXISTS (
            SELECT 1
            FROM "Order" o
            WHERE o.order_id = od.order_id
            AND o.customer_id = p_customer_id
        );

        RETURN v_cursor;
    END;
END SalesPackage;

create or replace procedure test_count()
as
v_total number(3);
begin
select count(*)into v_total from tb_order o left join TB_CUSTOMER c on o.customer_id=c.id where c.name=cusName;
DBMS_OUTPUT.put_line('总订单：'||v_total);
end;