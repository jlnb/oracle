# 20软工2班 202010414225 张凯凯

# 实验2：用户及权限管理

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

- 第1步：

```sql
$ sqlplus system/123@pdborcl
CREATE ROLE con;
GRANT connect,resource,CREATE VIEW TO con;
CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER saee default TABLESPACE "USERS";
ALTER USER saee QUOTA 50M ON users;
GRANT con TO saee;
--收回角色
REVOKE con FROM saee;
```

- 第2步：

```sql
$ sqlplus saee/123@pdborcl
SQL> show user;
USER is "saee"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar2(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
```

- 第3步：

```sql
用户hr连接到pdborcl，查询saee授予它的视图customers_view
$ sqlplus hr/123@pdborcl
SELECT * FROM saee.customers;
select * from saee.customers
SELECT * FROM saee.customers_view;
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
$ sqlplus system/123@pdborcl
SQL> alter user sale  account unlock;
```

- 第4步：

```sql
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```

- 第5步：

```sh
$ sqlplus system/123@pdborcl
SQL>
drop role con_res_role;
drop user sale cascade;
```

## 实验注意事项

- 完成时间：2023-04-25，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test2目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
